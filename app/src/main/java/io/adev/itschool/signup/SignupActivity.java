package io.adev.itschool.signup;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import io.adev.itschool.R;
import io.adev.itschool.network.ApiCaller;
import okhttp3.Response;

public class SignupActivity extends AppCompatActivity {

    private EditText mSignupName_editText;
    private EditText mSignupPassword_editText;
    private EditText mSignupGroup_editText;
    private EditText mSignupCity_editText;
    private Button mSignup_button;
    private String debugString = "nothing here yet";

    private TextView debug_text;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient mClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mSignupName_editText = (EditText) findViewById(R.id.signupName);
        mSignupPassword_editText = (EditText) findViewById(R.id.signupPassword);
        mSignupGroup_editText = (EditText) findViewById(R.id.signupGroup);
        mSignupCity_editText = (EditText) findViewById(R.id.signupCity);
        mSignup_button = (Button) findViewById(R.id.signupRegister_button);

        debug_text = (TextView) findViewById(R.id.debugText);
        debug_text.setText(debugString);

        mSignup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupPressed();
            }
        });
    }

    /*
    private void signupPressed() {
        String scannedNameText = mSignupName_editText.getText().toString();
        String scannedPasswordText = mSignupPassword_editText.getText().toString();
        String scannedGroupText = mSignupGroup_editText.getText().toString();
        String scannedCityText = mSignupCity_editText.getText().toString();
        try {
            Response response = ApiCaller.callSignup(scannedNameText, scannedPasswordText, scannedGroupText, scannedCityText);
            debug_text.setText(response.body().string());
        } catch (Exception e) {
            //return new ArrayList<>();
        }
        //String responseString = response.body().string();
    }
    */

    private void signupPressed() {
        final String scannedNameText = mSignupName_editText.getText().toString();
        final String scannedPasswordText = mSignupPassword_editText.getText().toString();
        final String scannedGroupText = mSignupGroup_editText.getText().toString();
        final String scannedCityText = mSignupCity_editText.getText().toString();

        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Response response = ApiCaller.callSignup(scannedNameText, scannedPasswordText, scannedGroupText, scannedCityText);
                    debugString = response.body().string();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

                return null;
            }
        };
    }
}
