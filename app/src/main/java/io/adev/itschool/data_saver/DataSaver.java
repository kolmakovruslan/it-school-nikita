package io.adev.itschool.data_saver;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Класс для сохранения и восстановления данных
 * Построен по принципу Map
 */
public class DataSaver {

    public static final String SCANNED_TEXT = "scanned";

    /**
     * Вызвать до первой попытки сохранить или восстановить данные
     */
    private static SharedPreferences sPreferences;
    public static void initIfNeeded(Context context) {

        if (sPreferences == null) {
            sPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

    }

    public static void put(String key, String value) {

        sPreferences
                .edit()
                .putString(key, value)
                .apply();

    }

    public static String get(String key) {
        return sPreferences.getString(key, null);
    }

}
