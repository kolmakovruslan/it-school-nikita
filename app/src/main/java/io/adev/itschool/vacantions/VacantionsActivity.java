package io.adev.itschool.vacantions;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.adev.itschool.R;

public class VacantionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacantions);
    }
}
