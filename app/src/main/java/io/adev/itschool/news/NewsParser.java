package io.adev.itschool.news;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.adev.itschool.news.model.NewModel;

/**
 * Класс, превращающий данные из сети
 * В список моделей
 */
public class NewsParser {

    public static List<NewModel> parse(String input) throws JSONException {

        JSONObject rootJson = new JSONObject(input);
        JSONArray newsArray = rootJson.getJSONArray("response");

        List<NewModel> newModels = new ArrayList<>();

        for (int i = 0; i < newsArray.length(); i = i + 1) {
            JSONObject newJson = newsArray.getJSONObject(i);
            NewModel newModel = new NewModel();

            newModel.title = newJson.getString("title");
            newModel.text = newJson.getString("text");

            newModels.add(newModel);
        }

        return newModels;
    }

}
