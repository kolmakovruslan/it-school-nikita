package io.adev.itschool.news;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.adev.itschool.R;
import io.adev.itschool.network.ApiCaller;
import io.adev.itschool.news.model.NewModel;
import okhttp3.Response;

/**
 * Логика отображения списка новостей
 */
public class NewsAdapter extends BaseAdapter {

    // Список моделей новостей
    private List<NewModel> mNewModels = new ArrayList<>();

    private Activity mActivity;
    public NewsAdapter(Activity activity) {
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mNewModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Вызывается когда необходимо отобразить элемент списка
     * @param position номер элемента в списке
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NewModel newModel = mNewModels.get(position);

        if (convertView == null) {
            // загрузить вёрстку из ресурса
            convertView = mActivity.getLayoutInflater().inflate(R.layout.item_news, null);
        }

        TextView title = (TextView) convertView.findViewById(R.id.new_title);
        TextView text = (TextView) convertView.findViewById(R.id.new_text);

        title.setText(newModel.title);
        text.setText(newModel.text);

        return convertView;
    }

    /**
     * Показать тестовые данные
     */
    public void update() {
        mNewModels = getData();
        notifyDataSetChanged();
    }

    /**
     * Абстрактный класс AsyncTask
     * реализует работу с другими потоками
     *
     * Все действия, связанные с UI
     * делаются в UI-потоке
     * Если начать делать какое-нибудь долгое действие
     * в этом потоке - приложение зависнет, пока это
     * действие не выполнится
     *
     * Обращение к сети - долгое действие
     * Поэтому его следует делать не в UI потоке
     *
     * Метод execute создаёт поток
     * После этого в другом потоке вызывается метод doInBackground
     *
     * Когда этот метод выполнился, в UI потоке
     * вызывается метод onPostExecute
     */
    public void updateAsync() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try {

                    Response response = ApiCaller.callNewsList(mActivity);
                    mNewModels = NewsParser.parse(response.body().string());

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                notifyDataSetChanged(); // обновить отображение списка
            }
        }.execute();

    }

    private List<NewModel> getData() {

        List<NewModel> newModels = new ArrayList<>();

        NewModel newModel1 = new NewModel();
        newModel1.title = "title 1";
        newModel1.text = "so long text that can not fit in one line. so long text that can not fit in one line. so long text that can not fit in one line. so long text that can not fit in one line";

        NewModel newModel2 = new NewModel();
        newModel2.title = "title 2";
        newModel2.text = "so long text that can not fit in one line. so long text that can not fit in one line. so long text that can not fit in one line. so long text that can not fit in one line";

        NewModel newModel3 = new NewModel();
        newModel3.title = "title 3";
        newModel3.text = "so long text that can not fit in one line. so long text that can not fit in one line. so long text that can not fit in one line. so long text that can not fit in one line";

        newModels.add(newModel1);
        newModels.add(newModel2);
        newModels.add(newModel3);

        return newModels;
    }

}
