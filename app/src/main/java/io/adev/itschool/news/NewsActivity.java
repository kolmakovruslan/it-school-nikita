package io.adev.itschool.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import io.adev.itschool.R;

/**
 * Экран отображения новостей
 */
public class NewsActivity extends AppCompatActivity {

    private ListView mList; // Отображение списка новостей
    private NewsAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        mAdapter = new NewsAdapter(this);

        mList = (ListView) findViewById(R.id.list);
        mList.setAdapter(mAdapter);

        mAdapter.update();
        mAdapter.updateAsync();
    }

}
