package io.adev.itschool.news.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Модель новости
 * Модели используются чтобы при работе с данными
 * не думать, как они будут отображаться
 * отображение впоследствии строится по моделям
 */
@Table(name = "NewModel")
public class NewModel extends Model {
    @Column(name = "title")
    public String title;
    @Column(name = "text")
    public String text;
}
