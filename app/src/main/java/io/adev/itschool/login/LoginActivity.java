package io.adev.itschool.login;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import io.adev.itschool.R;
import io.adev.itschool.data_saver.DataSaver;
import io.adev.itschool.network.ApiCaller;
import io.adev.itschool.news.NewsActivity;
import io.adev.itschool.signup.SignupActivity;
import okhttp3.Response;

/**
 * Экран логина
 */
public class LoginActivity extends AppCompatActivity {

    private EditText mUsernameTextEdit;
    private EditText mPasswordTextEdit;
    private Button mLoginButton;
    private Button mRegisterButton;
    private TextView mDebugText;

    private String debugString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        DataSaver.initIfNeeded(this);

        mUsernameTextEdit = (EditText)findViewById(R.id.username_editText);
        mPasswordTextEdit = (EditText) findViewById(R.id.password_editText);
        mLoginButton = (Button)   findViewById(R.id.login_button);
        mRegisterButton = (Button)   findViewById(R.id.signup_button);

        mDebugText = (TextView)findViewById(R.id.debugTextLogin);
        mDebugText.setText(debugString);


        String savedText = DataSaver.get(DataSaver.SCANNED_TEXT);
        mPasswordTextEdit.setText(savedText);


        // Можно реализовать все методы интерфейса, не создавая класс явно.
        // Класс всё равно создастся, но к нему нельзя будет обратиться впоследствии
        // Такой класс называется анонимным
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processClick();
            }
        });

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSignupActivity();
            }
        });

    }

    private void processClick() {
        final String scannedPasswordText = mPasswordTextEdit.getText().toString();
        final String scannedUsernameText = mUsernameTextEdit.getText().toString();
        //logining process
        //DataSaver.put(DataSaver.SCANNED_TEXT, scannedText);
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Response response = ApiCaller.callLogin(scannedUsernameText,scannedPasswordText);
                    Log.d("REQUEST",response.body().string());
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

                return null;
            }
        };
    }

    private void startSignupActivity() {
        startActivity(new Intent(this, SignupActivity.class)); // запуск нового активити
    }

}
