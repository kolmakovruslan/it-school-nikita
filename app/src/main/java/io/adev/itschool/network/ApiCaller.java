package io.adev.itschool.network;

import android.content.Context;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Класс обращения к сети
 */
public class ApiCaller {

    // Все запросы к сети делаются с помощью библиотеки OkHttp
    // Это очень популярная библиотека, которую поддерживают
    // многие другие библиотеки, связанные с работой с сетью
    private static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(   60, TimeUnit.SECONDS)
            .writeTimeout(  60, TimeUnit.SECONDS)
            .build();

    // адреса по которым сервер выводит нужные данные
    private static final String LOGIN_URL = "http://178.62.172.135:8080/itschool/login";

    // залогиниться
    public static Response callLogin(String username, String password) throws IOException {

        // составление ссылки для обращения к АПИ
        HttpUrl url = HttpUrl.parse(LOGIN_URL).newBuilder()
                .addEncodedQueryParameter("username", username)
                .addEncodedQueryParameter("password", password)
                .build();

        // составление запроса к серверу
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        // обращение к АПИ
        return OK_HTTP_CLIENT
                .newCall(request)
                .execute();
    }

    // зарегатся
    private static final String SIGNUP_URL = "178.62.172.135:8080/medapp/it_users/signup";
    public static Response callSignup(String username, String password,String group,String city) throws IOException {

        // составление ссылки для обращения к АПИ
        HttpUrl url = HttpUrl.parse(SIGNUP_URL).newBuilder()
                .addEncodedQueryParameter("username", username)
                .addEncodedQueryParameter("password", password)
                .addEncodedQueryParameter("group", group)
                .addEncodedQueryParameter("city", city)
                .build();

        // составление запроса к серверу
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        // обращение к АПИ
        return OK_HTTP_CLIENT
                .newCall(request)
                .execute();
    }

    private static final String NEWS_URL = "http://178.62.172.135:8080/medapp/it_news/get_all";
    public static Response callNewsList(Context context) throws IOException {

        HttpUrl url = HttpUrl.parse(NEWS_URL).newBuilder()
                .build();

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        return OK_HTTP_CLIENT
                .newCall(request)
                .execute();
    }

}
